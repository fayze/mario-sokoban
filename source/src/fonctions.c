#include <stdio.h>
#include "SDL.h"
#include "SDL_image.h"
#include "constantes.h"
#include "fonctions.h"

void pause()
{
    SDL_Event event;
    int q=1;
    while(q)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_KEYDOWN:
                q=0;
            break;
        default:
            break;

        }
    }
}


int afficher(int mat[NOMBRE_BLOC][NOMBRE_BLOC],SDL_Surface *ecran,SDL_Surface *mario[4],SDL_Surface *objectif,SDL_Surface *caisse[2],SDL_Surface *mur)
{
    SDL_Rect position={0};
    SDL_Surface *blanc=SDL_CreateRGBSurface(SDL_HWSURFACE,TAILLEBLOC,TAILLEBLOC,32,0,0,0,0);
    SDL_FillRect(blanc,NULL,SDL_MapRGB(ecran->format,COL1,COL2,COL3));
    int i=0,j=0;
    mario[UP]=IMG_Load("Images/mario_haut.gif");
    mario[DOWN]=IMG_Load("Images/mario_bas.gif");
    mario[LEFT]=IMG_Load("Images/mario_gauche.gif");
    mario[RIGHT]=IMG_Load("Images/mario_droite.gif");
    objectif=IMG_Load("Images/objectif.png");
    caisse[0]=IMG_Load("Images/caisse.JPG");
    caisse[1]=IMG_Load("Images/caisse_ok.JPG");
    mur=IMG_Load("Images/mur.jpg");
    for(i=0;i<NOMBRE_BLOC;i++)
    {
        for(j=0;j<NOMBRE_BLOC;j++)
        {
            position.x=j*TAILLEBLOC;
            position.y=i*TAILLEBLOC;
            switch(mat[i][j])
            {
            case UP:
                SDL_BlitSurface(mario[UP],NULL,ecran,&position);
                break;
            case DOWN:
                SDL_BlitSurface(mario[DOWN],NULL,ecran,&position);
                break;
            case LEFT:
                SDL_BlitSurface(mario[LEFT],NULL,ecran,&position);
                break;
            case RIGHT:
                SDL_BlitSurface(mario[RIGHT],NULL,ecran,&position);
                break;
            case BOX:
                SDL_BlitSurface(caisse[0],NULL,ecran,&position);
                break;
            case BOX_OK:
                SDL_BlitSurface(caisse[1],NULL,ecran,&position);
                break;
            case WALL:
                SDL_BlitSurface(mur,NULL,ecran,&position);
                break;
            case TARGET:
                SDL_BlitSurface(objectif,NULL,ecran,&position);
                break;
            case WHITE:
                SDL_BlitSurface(blanc,NULL,ecran,&position);
                break;

            default:
                printf("ERREUR: AUCUN OBJET CORRESPONDANT TROUV� DANS LA MATRICE (FONCTION AFFICHAGE)");
                return EXIT_FAILURE;
            break;

            }
        }
    }


    return EXIT_SUCCESS;
}


int deplacement(int mat[NOMBRE_BLOC][NOMBRE_BLOC],int *mem,int action)
{

    int i=0,j=0;
    int pos_i=0,pos_j=0;
    int booleen=1;
    int continuer=0,typeDeMouvement=AVANCER;

    for(i=0;i<NOMBRE_BLOC;i++)
    {
        for(j=0;j<NOMBRE_BLOC;j++)
        {
            if(mat[i][j]>=UP && mat[i][j]<=RIGHT)
                {
                    pos_i=i;
                    pos_j=j;
                    booleen=0;
                }
        }
    }

        if(booleen)
        {
            printf("\n\nMARIO INTROUVABLE");
            return EXIT_FAILURE;
        }

      switch(action)
      {
      case UP_ARROW:
          if(pos_i!=0 && mat[pos_i-1][pos_j]!=WALL)
        {

                if(mat[pos_i-1][pos_j]==WHITE || mat[pos_i-1][pos_j]==TARGET)
                {
                    continuer=1;
                    typeDeMouvement=AVANCER;

                }
                if(mat[pos_i-1][pos_j]==BOX || mat[pos_i-1][pos_j]==BOX_OK)
                {
                    if((pos_i-1)!=0 && mat[pos_i-2][pos_j]!=WALL && mat[pos_i-2][pos_j]!=BOX && mat[pos_i-2][pos_j]!=BOX_OK)
                    {
                        continuer=1;
                        typeDeMouvement=POUSSER;

                    }
                }

        }
        break;

      case DOWN_ARROW:
        if(pos_i!=NOMBRE_BLOC-1 && mat[pos_i+1][pos_j]!=WALL)
        {

                if(mat[pos_i+1][pos_j]==WHITE || mat[pos_i+1][pos_j]==TARGET)
                {
                    continuer=1;
                    typeDeMouvement=AVANCER;

                }
                if(mat[pos_i+1][pos_j]==BOX || mat[pos_i+1][pos_j]==BOX_OK)
                {
                    if((pos_i+1)!=NOMBRE_BLOC-1 && mat[pos_i+2][pos_j]!=WALL && mat[pos_i+2][pos_j]!=BOX && mat[pos_i+2][pos_j]!=BOX_OK)
                    {

                                    continuer=1;
                                    typeDeMouvement=POUSSER;

                    }
                }
        }
        break;

      case LEFT_ARROW:
        if(pos_j!=0 && mat[pos_i][pos_j-1]!=WALL)
        {
                if(mat[pos_i][pos_j-1]==WHITE || mat[pos_i][pos_j-1]==TARGET)
                {
                    continuer=1;
                    typeDeMouvement=AVANCER;

                }
                if(mat[pos_i][pos_j-1]==BOX || mat[pos_i][pos_j-1]==BOX_OK)
                {
                    if((pos_j-1)!=0 && mat[pos_i][pos_j-2]!=WALL && mat[pos_i][pos_j-2]!=BOX && mat[pos_i][pos_j-2]!=BOX_OK)
                    {
                        continuer=1;
                        typeDeMouvement=POUSSER;

                    }
                }
        }
        break;

      case RIGHT_ARROW:
            if(pos_j!=NOMBRE_BLOC-1 && mat[pos_i][pos_j+1]!=WALL)
        {

                if(mat[pos_i][pos_j+1]==WHITE || mat[pos_i][pos_j+1]==TARGET)
                {
                    continuer=1;
                    typeDeMouvement=AVANCER;

                }
                if(mat[pos_i][pos_j+1]==BOX || mat[pos_i][pos_j+1]==BOX_OK)
                {
                    if((pos_j+1)!=NOMBRE_BLOC-1 && mat[pos_i][pos_j+2]!=WALL && mat[pos_i][pos_j+2]!=BOX && mat[pos_i][pos_j+2]!=BOX_OK)
                    {
                        continuer=1;
                        typeDeMouvement=POUSSER;
                    }
                }
        }
        break;

      default:
        printf("\n\nTOUCHE APPUY�E NON RECONNUE 1 SORTIE.....");
        return EXIT_FAILURE;
        break;
      }

    if(continuer)
    {
        switch(action)
        {
            case LEFT_ARROW:
                if(typeDeMouvement==AVANCER)
                {
                    if(*mem==TARGET)
                    {
                        mat[pos_i][pos_j]=TARGET;

                    }
                    else
                    {
                        mat[pos_i][pos_j]=WHITE;

                    }
                    *mem=mat[pos_i][pos_j-1];
                    mat[pos_i][pos_j-1]=LEFT;
                }

                else if(typeDeMouvement==POUSSER)
                {
                    if(mat[pos_i][pos_j-1]==BOX)
                    {

                            if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=WHITE;
                        mat[pos_i][pos_j-1]=LEFT;
                        if(mat[pos_i][pos_j-2]==TARGET)
                        {
                            mat[pos_i][pos_j-2]=BOX_OK;
                        }
                        else if(mat[pos_i][pos_j-2]==WHITE)
                        {
                            mat[pos_i][pos_j-2]=BOX;
                        }
                    }
                    else if(mat[pos_i][pos_j-1]==BOX_OK)
                    {
                        if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=TARGET;
                        mat[pos_i][pos_j-1]=LEFT;
                        if(mat[pos_i][pos_j-2]==TARGET)
                        {
                            mat[pos_i][pos_j-2]=BOX_OK;
                        }
                        else if(mat[pos_i][pos_j-2]==WHITE)
                        {
                            mat[pos_i][pos_j-2]=BOX;
                        }
                    }
                }
                else
                    printf("\n\nMOUVEMENT NON RECONNU (PARTIE BOUGER MARIO)");
                    return EXIT_FAILURE;
            break;

            case RIGHT_ARROW:
                if(typeDeMouvement==AVANCER)
                {
                    if(*mem==TARGET)
                    {
                        mat[pos_i][pos_j]=TARGET;

                    }
                    else
                    {
                        mat[pos_i][pos_j]=WHITE;

                    }
                    *mem=mat[pos_i][pos_j+1];
                    mat[pos_i][pos_j+1]=RIGHT;
                }

                else if(typeDeMouvement==POUSSER)
                {
                    if(mat[pos_i][pos_j+1]==BOX)
                    {

                            if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=WHITE;
                        mat[pos_i][pos_j+1]=RIGHT;
                        if(mat[pos_i][pos_j+2]==TARGET)
                        {
                            mat[pos_i][pos_j+2]=BOX_OK;
                        }
                        else if(mat[pos_i][pos_j+2]==WHITE)
                        {
                            mat[pos_i][pos_j+2]=BOX;
                        }
                    }
                    else if(mat[pos_i][pos_j+1]==BOX_OK)
                    {
                        if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=TARGET;
                        mat[pos_i][pos_j+1]=RIGHT;
                        if(mat[pos_i][pos_j+2]==TARGET)
                        {
                            mat[pos_i][pos_j+2]=BOX_OK;
                        }
                        else if(mat[pos_i][pos_j+2]==WHITE)
                        {
                            mat[pos_i][pos_j+2]=BOX;
                        }
                    }
                }
                else
                    printf("\n\nMOUVEMENT NON RECONNU (PARTIE BOUGER MARIO)");
                    return EXIT_FAILURE;
                break;

            case UP_ARROW:
                if(typeDeMouvement==AVANCER)
                {
                    if(*mem==TARGET)
                    {
                        mat[pos_i][pos_j]=TARGET;

                    }
                    else
                    {
                        mat[pos_i][pos_j]=WHITE;

                    }
                    *mem=mat[pos_i-1][pos_j];
                    mat[pos_i-1][pos_j]=UP;
                }

                else if(typeDeMouvement==POUSSER)
                {
                    if(mat[pos_i-1][pos_j]==BOX)
                    {

                            if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=WHITE;
                        mat[pos_i-1][pos_j]=UP;
                        if(mat[pos_i-2][pos_j]==TARGET)
                        {
                            mat[pos_i-2][pos_j]=BOX_OK;
                        }
                        else if(mat[pos_i-2][pos_j]==WHITE)
                        {
                            mat[pos_i-2][pos_j]=BOX;
                        }
                    }
                    else if(mat[pos_i-1][pos_j]==BOX_OK)
                    {
                        if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=TARGET;
                        mat[pos_i-1][pos_j]=UP;
                        if(mat[pos_i-2][pos_j]==TARGET)
                        {
                            mat[pos_i-2][pos_j]=BOX_OK;
                        }
                        else if(mat[pos_i-2][pos_j]==WHITE)
                        {
                            mat[pos_i-2][pos_j]=BOX;
                        }
                    }
                }
                else
                    printf("\n\nMOUVEMENT NON RECONNU (PARTIE BOUGER MARIO)");
                    return EXIT_FAILURE;
                break;

            case DOWN_ARROW:
                if(typeDeMouvement==AVANCER)
                {
                    if(*mem==TARGET)
                    {
                        mat[pos_i][pos_j]=TARGET;

                    }
                    else
                    {
                        mat[pos_i][pos_j]=WHITE;

                    }
                    *mem=mat[pos_i+1][pos_j];
                    mat[pos_i+1][pos_j]=DOWN;
                }

                else if(typeDeMouvement==POUSSER)
                {
                    if(mat[pos_i+1][pos_j]==BOX)
                    {

                            if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=WHITE;
                        mat[pos_i+1][pos_j]=DOWN;
                        if(mat[pos_i+2][pos_j]==TARGET)
                        {
                            mat[pos_i+2][pos_j]=BOX_OK;
                        }
                        else if(mat[pos_i+2][pos_j]==WHITE)
                        {
                            mat[pos_i+2][pos_j]=BOX;
                        }
                    }
                    else if(mat[pos_i+1][pos_j]==BOX_OK)
                    {
                        if(*mem==TARGET)
                        {
                            mat[pos_i][pos_j]=TARGET;

                        }
                            else
                        {
                            mat[pos_i][pos_j]=WHITE;

                        }
                        *mem=TARGET;
                        mat[pos_i+1][pos_j]=DOWN;
                        if(mat[pos_i+2][pos_j]==TARGET)
                        {
                            mat[pos_i+2][pos_j]=BOX_OK;
                        }
                        else if(mat[pos_i+2][pos_j]==WHITE)
                        {
                            mat[pos_i+2][pos_j]=BOX;
                        }
                    }
                }
                else
                    printf("\n\nMOUVEMENT NON RECONNU (PARTIE BOUGER MARIO)");
                    return EXIT_FAILURE;
                break;
            default:
                printf("\n\nTOUCHE APPUY�E NON RECONNUE 2 SORTIE.....");
                return EXIT_FAILURE;
            break;



        }

    }//fin continuer


    return EXIT_SUCCESS;

}//fin fonction

int gagne(int mat[NOMBRE_BLOC][NOMBRE_BLOC],int mem)
{
    int win=1;
    int i=0,j=0;
    for(i=0;i<NOMBRE_BLOC;i++)
    {
        for(j=0;j<NOMBRE_BLOC;j++)
        {
            if(mat[i][j]==TARGET)
                win=0;
        }
    }
    if(mem==TARGET)
        win=0;

    return win;
}


