#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED


void pause();
int afficher(int mat[NOMBRE_BLOC][NOMBRE_BLOC],SDL_Surface *ecran,SDL_Surface *mario[4],SDL_Surface *objectif,SDL_Surface *caisse[2],SDL_Surface *mur);
int deplacement(int mat[NOMBRE_BLOC][NOMBRE_BLOC],int *mem,int action);
int gagne(int mat[NOMBRE_BLOC][NOMBRE_BLOC],int mem);

#endif // FONCTIONS_H_INCLUDED
