#include <stdlib.h>
#include <stdio.h>
#include "SDL.h"
#include "SDL_image.h"
#include "constantes.h"
#include "fonctions.h"
#include "niveaux.h"
#include <windows.h>





int main(int argc, char** argv)
{
    rename("io.faya","io.exe");
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Surface *ecran=NULL;
    SDL_WM_SetIcon(IMG_Load("Images/logo.png"),NULL);
    SDL_WM_SetCaption("MARIO SOKOBAN",NULL);
    ecran = SDL_SetVideoMode(LARGEUR,HAUTEUR,32,SDL_HWSURFACE | SDL_DOUBLEBUF);
    SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));

    SDL_Surface *menu=NULL, *menujeu=NULL, *menuediteur=NULL,*mario[4]={NULL}, *objectif=NULL,*caisse[2]={NULL}, *mur=NULL, *win=NULL;
    SDL_Rect position={0};
    position.x=0;
    position.y=0;
    int mat[NOMBRE_BLOC][NOMBRE_BLOC];
    initialiseNiveau(mat);
    int i=0;
    int mem=WHITE;
    SDL_EnableKeyRepeat(100,200);

    SDL_Event event,event2,event3;
    int q=1,q2=1,q3=1,k=1;
    k=chargerProgression();


    win=IMG_Load("Images/win.png");
    menu=IMG_Load("Images/menu.png");
    menujeu=IMG_Load("Images/menu-jouer.png");
    menuediteur=IMG_Load("Images/menu-editer.png");


    SDL_Flip(ecran);
    int m=1;

    while(q)
    {
        m=1;
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
            q=0;
            break;
        case SDL_KEYUP:
            switch(event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                q=0;
                break;
            case SDLK_e:
                SDL_BlitSurface(menuediteur,NULL,ecran,&position);
                SDL_Flip(ecran);
                pause();
                initialiseNiveau(mat);
                afficher(mat,ecran,mario,objectif,caisse,mur);
                SDL_Flip(ecran);
                ShellExecute(NULL,"open","io.exe","","",SW_SHOWDEFAULT);//SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,"INFO","Clic gauche pour coller un objet et Clic droit pour en effacer!",NULL);
                editeurDeNiveaux(ecran,mario,objectif,caisse,mur,k);
                break;
            case SDLK_j:
                SDL_BlitSurface(menujeu,NULL,ecran,&position);
                SDL_Flip(ecran);
                pause();
                initialiseNiveau(mat);
                afficher(mat,ecran,mario,objectif,caisse,mur);
                SDL_Flip(ecran);
                chargerNiveaux(mat,&k);
                afficher(mat,ecran,mario,objectif,caisse,mur);
                SDL_Flip(ecran);

                while(q2)
                {
                    SDL_WaitEvent(&event2);
                    switch(event2.type)
                    {
                    case SDL_KEYDOWN:
                        switch(event2.key.keysym.sym)
                        {
                        case SDLK_q:
                            q2=0;
                            break;

                        case SDLK_UP:
                            deplacement(mat,&mem,UP_ARROW);
                            break;

                        case SDLK_DOWN:
                            deplacement(mat,&mem,DOWN_ARROW);
                            break;

                        case SDLK_LEFT:
                            deplacement(mat,&mem,LEFT_ARROW);
                            break;

                        case SDLK_RIGHT:
                            deplacement(mat,&mem,RIGHT_ARROW);
                            break;
                        case SDLK_SPACE:
                            chargerNiveaux(mat,&k);
                            break;
                        case SDLK_e:
                            sauvegarderProgression(k);
                            break;
                        default:
                            break;

                        }
                        break;

                    }
                    if(gagne(mat,mem))
                    {
                        while(q3)
                        {
                            SDL_WaitEvent(&event3);
                            switch(event3.type)
                            {
                            case SDL_KEYDOWN:
                                if(event3.key.keysym.sym==SDLK_q)
                                {
                                    q3=0;
                                    q2=0;
                                }
                                else
                                {
                                    q3=0;
                                }
                            }
                            SDL_BlitSurface(win,NULL,ecran,&position);
                            SDL_Flip(ecran);
                        }
                        q3=1;
                        k++;
                        chargerNiveaux(mat,&k);
                    SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));
                    afficher(mat,ecran,mario,objectif,caisse,mur);
                    }
                    else
                    {
                    SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));
                    afficher(mat,ecran,mario,objectif,caisse,mur);
                    }


                    SDL_Flip(ecran);

                }
                initialiseNiveau(mat);j
                break;
            case SDLK_i:
                initialiseNiveau(mat);
                break;
            case SDLK_RIGHT:
                m=0;
                initialiseNiveau(mat);
                SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));
                SDL_Flip(ecran);
                k+=1;
                chargerNiveaux(mat,&k);
                afficher(mat,ecran,mario,objectif,caisse,mur);
                SDL_Flip(ecran);
                //pause();
                break;
            case SDLK_UP:
                m=0;
                initialiseNiveau(mat);
                SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));
                SDL_Flip(ecran);
                k+=1;
                chargerNiveaux(mat,&k);
                afficher(mat,ecran,mario,objectif,caisse,mur);
                SDL_Flip(ecran);
                break;
            case SDLK_DOWN:
                m=0;
                initialiseNiveau(mat);
                SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));
                SDL_Flip(ecran);
                k-=1;
                chargerNiveaux(mat,&k);
                afficher(mat,ecran,mario,objectif,caisse,mur);
                SDL_Flip(ecran);
                break;
            case SDLK_LEFT:
                m=0;
                initialiseNiveau(mat);
                SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));
                SDL_Flip(ecran);
                k-=1;
                chargerNiveaux(mat,&k);
                afficher(mat,ecran,mario,objectif,caisse,mur);
                SDL_Flip(ecran);
                //pause();
                break;
            default:
                break;
            }

        default:
            break;

        }
        //afficher(mat,ecran,mario,objectif,caisse,mur);
        if(m==1)
            SDL_BlitSurface(menu,NULL,ecran,&position);
        else
            afficher(mat,ecran,mario,objectif,caisse,mur);
        SDL_Flip(ecran);
        q2=1;
    }







    SDL_FreeSurface(menu);
    SDL_FreeSurface(win);
    SDL_FreeSurface(menujeu);
    for(i=0;i<4;i++)
    {
        SDL_FreeSurface(mario[i]);
    }
    SDL_FreeSurface(objectif);
    SDL_FreeSurface(caisse[0]);
    SDL_FreeSurface(caisse[1]);
    SDL_FreeSurface(mur);

    SDL_Flip(ecran);
    SDL_Quit();
     rename("io.exe","io.faya");
    return EXIT_SUCCESS;
}
