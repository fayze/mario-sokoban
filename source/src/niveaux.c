#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>
#include "constantes.h"
#include "fonctions.h"
#include "niveaux.h"


void sauverNiveau(int mat[NOMBRE_BLOC][NOMBRE_BLOC])
{
    int i=0,j=0;
    FILE *fichier=NULL;
    fichier=fopen("fichiers/niveau.faya","a");
    fprintf(fichier,"\n");
    for(i=0;i<NOMBRE_BLOC;i++)
    {
        for(j=0;j<NOMBRE_BLOC;j++)
        {
            fprintf(fichier,"%d",mat[i][j]);
        }
    }

    fclose(fichier);
}

void blitmotion(SDL_Surface *ecran,int object,SDL_Rect position)
{
    SDL_Surface *objet=NULL;

    switch(object)
    {
        case UP:
        objet=IMG_Load("Images/mario_haut.gif");
            break;

        case DOWN:
            objet=IMG_Load("Images/mario_bas.gif");
            break;

        case LEFT:
            objet=IMG_Load("Images/mario_gauche.gif");
            break;

        case RIGHT:
            objet=IMG_Load("Images/mario_droite.gif");
            break;

        case BOX:
            objet=IMG_Load("Images/caisse.jpg");
            break;

        case BOX_OK:
            objet=IMG_Load("Images/caisse_ok.jpg");
            break;

        case WALL:
            objet=IMG_Load("Images/mur.jpg");
            break;

        case TARGET:
            objet=IMG_Load("Images/objectif.png");
            break;

        default:
            printf("\n\nBLITMOTION OBJECT NON RECONNU");
            break;
    }

    SDL_BlitSurface(objet,NULL,ecran,&position);


}

int chercherMario(int mat[NOMBRE_BLOC][NOMBRE_BLOC])
{

    int i=0,j=0;
    int trouve=0;

    for(i=0;i<NOMBRE_BLOC;i++)
    {
        for(j=0;j<NOMBRE_BLOC;j++)
        {
            if(mat[i][j]>=UP && mat[i][j]<=RIGHT)
                trouve++;
        }
    }

    return trouve;
}


int nombreDeLignes()
{

    FILE *fichier=fopen("fichiers/niveau.faya","r");
    int n=0,c;

    while((c=fgetc(fichier))!=EOF)
    {
        if(c=='\n')
            n++;
    }
    n++;

    fclose(fichier);
    return n;
}

void chargerNiveaux(int mat[NOMBRE_BLOC][NOMBRE_BLOC],int *k)
{

    int i=0,j=0,z=0,limite=0;
    limite=nombreDeLignes();

    if(*k<1)
    {
        *k=limite;
    }
    else if(*k>limite)
    {
        *k=1;
    }


    FILE *fichier=NULL;
    fichier=fopen("fichiers/niveau.faya","r");
    char carte[NOMBRE_BLOC*NOMBRE_BLOC+2]={0};
    if (fichier == NULL)
        printf("erreur d' ouverture du fichier");

        for(z=0;z<*k;z++)
        {
            carte[0]='\0';
            fgets(carte,NOMBRE_BLOC*NOMBRE_BLOC+2,fichier);
        }


    for(i=0;i<NOMBRE_BLOC;i++)
    {
        for(j=0;j<NOMBRE_BLOC;j++)
        {
            switch(carte[(i*NOMBRE_BLOC)+j])
            {
            case '0':
                mat[i][j]=UP;
            break;

            case '1':
                mat[i][j]=DOWN;
            break;

            case '2':
                mat[i][j]=LEFT;
                break;

            case '3':
                mat[i][j]=RIGHT;
                break;

            case '4':
                mat[i][j]=BOX;
                break;

            case '5':
                mat[i][j]=BOX_OK;
                break;

            case '6':
                mat[i][j]=WALL;
                break;

            case '7':
                mat[i][j]=TARGET;
                break;

            case '8':
                mat[i][j]=WHITE;
                break;
            default:
                printf("\n\n------------------- erreur de lecture du de la carte --------------");
                break;
            }
        }
    }


    fclose(fichier);
}


void initialiseNiveau(int mat[NOMBRE_BLOC][NOMBRE_BLOC])
{
        int i=0,j=0;
        for(i=0;i<NOMBRE_BLOC;i++)
        {
            for(j=0;j<NOMBRE_BLOC;j++)
            {
                mat[i][j]=WHITE;
            }
        }

}

void editeurDeNiveaux(SDL_Surface *ecran,SDL_Surface *mario[4],SDL_Surface *objectif,SDL_Surface *caisse[2],SDL_Surface *mur,int k)
{
    SDL_Event event;
    int mat[NOMBRE_BLOC][NOMBRE_BLOC];
    chargerNiveaux(mat,&k);
    afficher(mat,ecran,mario,objectif,caisse,mur);
    SDL_Flip(ecran);
    int rightclicInProgress=0,leftClicInProgress=0;
    int q=1;
    int objet=WALL;
    int trouve=chercherMario(mat);


    SDL_Rect position={0};

    while(q)
    {
        trouve=chercherMario(mat);
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
            q=0;
            break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym)
            {
            case SDLK_1:
                objet=UP;
                break;
            case SDLK_2:
                objet=DOWN;
                break;
            case SDLK_3:
                objet=LEFT;
                break;
            case SDLK_4:
                objet=RIGHT;
                break;
            case SDLK_5:
                objet=BOX;
                break;
            case SDLK_6:
                objet=BOX_OK;
                break;
            case SDLK_7:
                objet=WALL;
                break;
            case SDLK_8:
                objet=TARGET;
                break;
             case SDLK_s:
                sauverNiveau(mat);
                break;
             case SDLK_q:
                q=0;
                break;
            default:
                break;
            }

            break;

            case SDL_MOUSEBUTTONDOWN:
                switch(event.button.button)
                {
                case SDL_BUTTON_LEFT:
                    leftClicInProgress=1;
                    if(!(objet>=UP &&objet<=RIGHT && trouve>0))
                        mat[event.button.y/TAILLEBLOC][event.button.x/TAILLEBLOC]=objet;
                    break;
                case SDL_BUTTON_RIGHT:
                    rightclicInProgress=1;
                    mat[event.button.y/TAILLEBLOC][event.button.x/TAILLEBLOC]=WHITE;
                    break;
                default:
                    printf("ERREUR CLIC DE SOURIS NON DETERMINE");
                    break;
                }
                break;

            case SDL_MOUSEBUTTONUP:
                    if(event.button.button==SDL_BUTTON_LEFT)
                        leftClicInProgress=0;
                    else if(event.button.button==SDL_BUTTON_RIGHT)
                        rightclicInProgress=0;
                break;

            case SDL_MOUSEMOTION:
                            position.x=event.motion.x-(TAILLEBLOC/2);
                        position.y=event.motion.y-(TAILLEBLOC/2 );
                    if(leftClicInProgress)
                    {

                        if(!(objet>=UP &&objet<=RIGHT && trouve>0))
                            mat[event.button.y/TAILLEBLOC][event.button.x/TAILLEBLOC]=objet;
                    }

                    else if(rightclicInProgress)
                    {
                            //position.x=event.motion.x-(TAILLEBLOC/2);
                            //position.y=event.motion.y-(TAILLEBLOC/2 );
                            mat[event.button.y/TAILLEBLOC][event.button.x/TAILLEBLOC]=WHITE;
                    }

                break;

        default:
            break;
        }
        SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format,COLFONDDECRAN,COLFONDDECRAN,COLFONDDECRAN));
        afficher(mat,ecran,mario,objectif,caisse,mur);
        blitmotion(ecran,objet,position);
        SDL_Flip(ecran);
    }
}

void sauvegarderProgression(int k)
{

    FILE *fichier=fopen("fichiers/Progress.faya","w+");
    fprintf(fichier,"%d",k);

    fclose(fichier);


}
int chargerProgression()
{
    int k=1;
    FILE *fichier=fopen("fichiers/Progress.faya","r");
    fscanf(fichier,"%d",&k);
    fclose(fichier);
    if(!(k>=1 && k<=nombreDeLignes()))
        k=1;
    return k;

}

