#ifndef NIVEAUX_H_INCLUDED
#define NIVEAUX_H_INCLUDED


int nombreDeLignes();
void blitmotion(SDL_Surface *ecran,int object,SDL_Rect position);
int chercherMario(int mat[NOMBRE_BLOC][NOMBRE_BLOC]);
void chargerNiveaux(int mat[NOMBRE_BLOC][NOMBRE_BLOC],int *k);
void sauverNiveau(int mat[NOMBRE_BLOC][NOMBRE_BLOC]);
void initialiseNiveau(int mat[NOMBRE_BLOC][NOMBRE_BLOC]);
void editeurDeNiveaux(SDL_Surface *ecran,SDL_Surface *mario[4],SDL_Surface *objectif,SDL_Surface *caisse[2],SDL_Surface *mur,int k);
void sauvegarderProgression(int k);
int chargerProgression(void);



#endif // NIVEAUX_H_INCLUDED
